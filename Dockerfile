FROM golang:1.21 AS builder
WORKDIR /app
COPY . .
RUN CGO_ENABLED=0 GO111MODULE=off go build -o hello

FROM alpine:3.18.5
WORKDIR /app
COPY --from=builder /app/hello .
CMD ["./hello"]
